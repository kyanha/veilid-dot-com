---
title: Translations
description: Help translate documentation and more for the Veilid project
layout: index
exclude: true
---


<div class="row g-5">
  <div class="col-12 col-md-7">
    <p>
      We are very early on in this project and currently only have our text in English.
    </p>
    <p>
      We are seeking volunteers to help with this project's translations.
    </p>
    <p>
      Please join us on our server to talk about how we can expand our reach.
    </p>
  </div>
  <div class="col-12 col-md-5">
    <h3 class="mt-0 text-center">Help Wanted</h3>
    <p>
      <a href="/discord" class="btn btn-primary btn-lg w-100">Veilid Discord</a>
    </p>
  </div>
</div>



